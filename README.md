# Analytics Playground

## Stack Components

1. [Vector](https://vector.dev/)
1. [Clickhouse](https://clickhouse.com/)
1. [Tabix](https://tabix.io/)
1. [Grafana](https://grafana.com/)

## Usage

```sh
# Start Clickhouse for data storage
$ make clickhouse

# Start Vector for event ingress
$ make vector

# Start Tabix for ad-hoc queries (available on localhost:9000)
$ make tabix

# Start Grafana for dashboard views (available on localhost:3000)
$ make grafana
```

Submit an event to Vector

```sh
REQ_METHODS=("GET" "POST")
REQ_HOSTS=("www.a.com" "www.b.com")
REQ_PATHS=("/" "/a" "/b" "/c")
STATUS_CODES=(200 301 404 500 503)
CACHE_STATUSES=("HIT" "MISS" "PASS")
CITIES=("Chicago" "Seattle" "Chapel Hill")

while true; do
    RAND_N=${RANDOM}

    DURATION_MICROSECOND=${RAND_N}
    REQ_METHOD="${REQ_METHODS[1 + $RANDOM % ${#REQ_METHODS[@]}]}"
    REQ_HOST="${REQ_HOSTS[1 + $RANDOM % ${#REQ_HOSTS[@]}]}"
    REQ_PATH="${REQ_PATHS[1 + $RANDOM % ${#REQ_PATHS[@]}]}"
    STATUS_CODE="${STATUS_CODES[1 + $RANDOM % ${#STATUS_CODES[@]}]}"
    CACHE_STATUS="${CACHE_STATUSES[1 + $RANDOM % ${#CACHE_STATUSES[@]}]}"
    CITY="${CITIES[1 + $RANDOM % ${#CITIES[@]}]}"

    curl -sL -X POST \
        http://localhost:8080 \
        --data @- << EOF
{
    "start_ms": $(($(date +%s) * 1000)),
    "duration_us": ${DURATION_MICROSECOND},
    "req.method": "${REQ_METHOD}",
    "req.host": "${REQ_HOST}",
    "req.path": "${REQ_PATH}",
    "resp.status_code": ${STATUS_CODE},
    "cache_status": "${CACHE_STATUS}",
    "geo_city": "${CITY}",
    "geo_country_code": "US",
    "geo_country_name": "United States"
}
EOF

    sleep 1
done
```
