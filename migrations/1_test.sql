CREATE TABLE IF NOT EXISTS test (
    `test` String,
    `date` DateTime
)
ENGINE = MergeTree()
PARTITION BY toYYYYMM(date)
ORDER BY date;
