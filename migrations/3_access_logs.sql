CREATE TABLE IF NOT EXISTS access_logs (
    `start_ms`    UInt64,
    `duration_us` UInt64,

    `is_tls`  UInt8,
    `is_h2`   UInt8,
    `is_ipv6` UInt8,

    `req.method`          String,
    `req.proto`           String,
    `req.proto_ver`       String,
    `req.path`            String,
    `req.query`           String,
    `req.bytes_read`      UInt32,
    `req.original_host`   String,
    `req.host`            String,
    `req.referer`         String,
    `req.user_agent`      String,
    `req.accept`          String,
    `req.accept_language` String,
    `req.accept_encoding` String,
    `req.forwarded`       String,
    `req.via`             String,
    `req.x_requested_for` String,

    `resp.status_code`      UInt32,
    `resp.bytes_written`    UInt32,
    `resp.content_type`     String,
    `resp.cache_control`    String,
    `resp.age`              String,
    `resp.expires`          String,
    `resp.last_modified`    String,
    `resp.etag`             String,
    `resp.content_encoding` String,
    `resp.content_length`   UInt32,

    `cache_status` String,
    `is_cacheable` UInt8,

    `client_ip`          String,
    `geo_city`           String,
    `geo_country_code`   String,
    `geo_country_name`   String,

    `date` DateTime DEFAULT now()
)
ENGINE = MergeTree()
PARTITION BY toYYYYMM(date)
ORDER BY date;
