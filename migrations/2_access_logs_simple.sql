CREATE TABLE IF NOT EXISTS access_logs_simple (
    `start_ms`    UInt64,
    `duration_us` UInt64,

    `req.method`       String,
    `req.host`         String,
    `req.path`         String,
    `resp.status_code` UInt32,

    `cache_status` String,

    `geo_city`         String,
    `geo_country_code` String,
    `geo_country_name` String,

    `date` DateTime DEFAULT now()
)
ENGINE = MergeTree()
PARTITION BY toYYYYMM(date)
ORDER BY date;
