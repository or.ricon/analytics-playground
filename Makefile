NETWORK_NAME ?= analytics

VECTOR_VERSION     ?= 0.24.1-debian
CLICKHOUSE_VERSION ?= 22.8.5
GRAFANA_VERSION    ?= 9.1.5
TABIX_VERSION	   ?= 22.05.17

network:
	docker network create analytics

.PHONY: clickhouse
clickhouse:
	docker run \
		--rm -it \
		--name clickhouse \
		--network $(NETWORK_NAME) \
		-p 8123:8123 \
		-v $(PWD)/clickhouse/data:/bitnami/clickhouse \
		-e ALLOW_EMPTY_PASSWORD=yes \
		bitnami/clickhouse:$(CLICKHOUSE_VERSION)

.PHONY: vector
vector:
	docker run \
		--rm -it \
		--name vector \
		--network $(NETWORK_NAME) \
		-p 8080:80 \
		-v $(PWD)/vector/vector.toml:/etc/vector/vector.toml:ro \
		timberio/vector:$(VECTOR_VERSION)

.PHONY: grafana
grafana:
	docker run \
		--rm -it \
		--name grafana \
		--network $(NETWORK_NAME) \
		-p 3000:3000 \
		-v $(PWD)/grafana/data:/var/lib/grafana \
		-e GF_INSTALL_PLUGINS=grafana-clickhouse-datasource \
		grafana/grafana:$(GRAFANA_VERSION)

.PHONY: tabix
tabix:
	docker build -t tabix:$(TABIX_VERSION) -f tabix/Dockerfile --build-arg TABIX_VERSION=$(TABIX_VERSION) .
	docker run \
		--rm -it \
		--name tabix \
		-p 9000:9000 \
		tabix:$(TABIX_VERSION)
